function ret = loaddata(loaddata_name, loaddata_start, loaddata_stop)
% This function loads a data file (space separated).
%   
%   LOADDATA(filename)
%   LOADDATA(filename, startline, endline)
%
%   The function populates the environment with the content of the file.
%   Variable names are taken from the first line of the file.
%   Only a certain portion of the file can be selected.
%

    % cut file if requested
    if exist('loaddata_start', 'var') && exist('loaddata_stop', 'var')
        loaddata_cut = 1;
    else
        loaddata_cut = 0;
    end
    
    loaddata_columns = importdata(loaddata_name);

    % check if there is anything wrong with the import
    if (~isfield(loaddata_columns, 'colheaders'))
        fprintf('Error in loading file.\n');
        ret = 0;
        return;
    end

    cols = size(loaddata_columns.colheaders);
    cols = cols(2);

    % get variable names
    loaddata_names = genvarname(loaddata_columns.colheaders, evalin('caller', 'who'));

    % put data in the variables
    for i=1:cols
        loaddata_v = loaddata_names{i};
        eval(['global ' loaddata_v]);
        if (loaddata_cut == 1)
            eval([loaddata_v '= loaddata_columns.data(loaddata_start:loaddata_stop, i);']);
        else
            eval([loaddata_v '= loaddata_columns.data(:, i);']);
        end
        evalin('caller', ['global ' loaddata_v]);
    end

    ret = 1;

end