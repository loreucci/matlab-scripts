function rasterplot(ids, times)

scatter(times/1000, ids, '.');
xlabel('Time (s)');
ylabel('Neuron ID');

end