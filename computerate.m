function computerate(IDs, times, binsize, minID, maxID)

    bins = zeros(floor(max(times)/binsize)+1, 1);

%     bins = 0:binsize:max(times);
    
    for i=1:length(IDs)
        
        if (minID <= IDs(i) && IDs(i) < maxID)
            
            bins(floor(times(i)/binsize)+1) = bins(floor(times(i)/binsize)+1) + 1;
            
        end
        
    end
    
    plot((0:binsize:max(times))/1000, bins/(maxID-minID)/binsize*1000, 'Linewidth', 2);
%     plot(bins/binsize);
    
end